module.exports = function (config) {
  config.set({
    ...
    browsers: ["Chrome"],
    customLaunchers: {
      Headless_Chrome: {
        base: "Chrome",
        flags: ["--no-sandbox", "--disable-gpu"],
      },
    }
  });
};
