FROM node:latest

WORKDIR /app
ENV PATH /APP/node_modules/.bin:$PATH

COPY package.json /app/package.json
RUN npm install
RUN npm install -g @angular/cli@16.1.8

COPY . /app
CMD ng serve --host 0.0.0.0 --port 4200